# SlashGaming Diablo II Modding API
Copyright (C) 2018  SlashGaming Community

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Additional permissions under GNU Affero General Public License version 3
section 7

If you modify this Program, or any covered work, by linking or combining
it with Diablo II (or a modified version of that library), containing
parts covered by the terms of Blizzard End User License Agreement, the
licensors of this Program grant you additional permission to convey the
resulting work.

If you modify this Program, or any covered work, by linking or combining
it with Sven Labusch's GLIDE3-to-OpenGL-Wrapper (or a modified version
of that library), the licensors of this Program grant you additional
permission to convey the resulting work.

If you modify this Program, or any covered work, by linking or combining
it with Zeus Software's nGlide (or a modified version of that library),
the licensors of this Program grant you additional permission to convey
the resulting work.
